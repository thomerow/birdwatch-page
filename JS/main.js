(function() {
	"use strict";

	var isGetting = false;

	function getImage() {
		isGetting = true;

		var btnCapt = document.getElementById('btnCapture');
		btnCapt.disabled = true;
		var httpc = new XMLHttpRequest();
		var url = 'capture.php';
		httpc.open('GET', url, true);
		httpc.onreadystatechange = function() {
			if ((httpc.readyState == 4) && (httpc.status == 200)) {
				var img = document.getElementById('capture');
				img.src = httpc.responseText;				
				img.style.opacity = 1;
				btnCapt.disabled = false;
				isGetting = false;			
			}
		};
		httpc.send(null);
	}

	function btnCaptureClick(evt) {
		if (isGetting) return;
		var btnCapt = document.getElementById('btnCapture');
		btnCapt.disabled = true;
		var img = document.getElementById('capture');
		img.style.opacity = 0;
		getImage();		
	}
	
	function main() {
		// Attach click event handler to button
		var btn = document.getElementById('btnCapture');
		btn.addEventListener('click', btnCaptureClick);

		// Retrieve first image
		getImage();
	}

	window.addEventListener('load', main);
})();

